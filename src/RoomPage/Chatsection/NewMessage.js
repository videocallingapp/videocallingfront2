import React, { useEffect, useState } from "react";
import SendRoundedIcon from "@mui/icons-material/SendRounded";
import * as WebRTCHandler from "../../utils/WebRTCHandler";
import { Button } from "@material-ui/core";
import Emoji from "../../Emoji/Emoji";
import CloseIcon from "@material-ui/icons/Close";
import EmojiEmotionsOutlinedIcon from "@material-ui/icons/EmojiEmotionsOutlined";
import { uploadBytes, getDownloadURL } from "../../firebase/firebase";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { getStorage, ref } from "firebase/storage";
import "./NewMessage.css";

export default function NewMessage() {
  const [emojiTemplate, setEmojiTemplate] = useState(false);

  const [message, setNewMessage] = useState("");

  const [file, setFile] = useState(null);

  useEffect(() => {
    if (file != null) uploadFileToFirebase();
  }, [file]);

  const [showfile, setShowfile] = useState(false);
  const handleTextChange = (event) => {
    setNewMessage(event.target.value);
  };

  function setEmoji(Emoji) {
    setNewMessage(message + Emoji);
  }
  const handleKeyPress = (event) => {
    if (event.key === "Enter" && message) {
      event.preventDefault();
      // console.log('sending message to other');
      // console.log(message);
      sendMessage();
    }
  };
  const sendMessage = () => {
    if (message.length > 0) {
      WebRTCHandler.sendMessageUsingDataChannel(message, "text", "default");
      setNewMessage("");
      setEmojiTemplate(false);
    }
  };
  const onFileChange = async (e) => {
    const reader = new FileReader();
    let file = e.target.files[0]; // get the supplied file
    // if there is a file, set image to that file
    if (file) {
      reader.onload = async () => {
        if (reader.readyState === 2) {
          setFile(file);
        }
      };
      reader.readAsDataURL(e.target.files[0]);
    } else {
      setFile(null);
    }
  };
  const uploadFileToFirebase = async () => {
    //1.
    if (file) {
      console.log(file.name);
      //2.
      //   setLoader(true);
      //3.
      const storage = getStorage();
      const storageRef = ref(storage, file.name);
      await uploadBytes(storageRef, file).then((snapshot) => {
        console.log("Uploaded a blob or file!");
        console.log(snapshot);
      });

      await getDownloadURL(ref(storage, file.name))
        .then((url) => {
          // `url` is the download URL for 'images/stars.jpg'
          setEmojiTemplate(false);
          WebRTCHandler.sendMessageUsingDataChannel(url, "file", file.name);
          // This can be downloaded directly
        })
        .catch((error) => {
          // Handle any errors
          console.log("====================================");
          console.log("error in accessing file url: " + error);
          console.log("====================================");
        });
      setFile(null);
    } else {
      alert("Please upload a document first.");
    }
    setShowfile(false);
  };

  return (
    <div className="new_message_container">
      {emojiTemplate ? (
        <>
          <Emoji setEmoji={setEmoji} />
          <Button
            onClick={() => {
              setEmojiTemplate(false);
            }}
          >
            <CloseIcon />
          </Button>
        </>
      ) : (
        // <Button

        // >
        <EmojiEmotionsOutlinedIcon
          style={{ fontSize: "30px" }}
          onClick={() => {
            setEmojiTemplate(true);
          }}
        />
        // </Button>
      )}
      {/* {showfile ? ( */}
      <>
        {/* <Button> */}
        <span class="fileUpload">
          <AttachFileIcon
            className="attach-file"
            style={{ fontSize: "27px" ,marginRight: "10px" }}
          />
          <input
            type="file"
            className="file-upload"
            id="file-upload"
            onChange={(e) => {
              onFileChange(e);
            }}
          />
        </span>
        {/* </Button> */}
        {/* <Button>
            {" "}
            <CloudUploadIcon onClick={uploadFileToFirebase} />{" "}
          </Button> */}
        {/* <Button
            onClick={() => {
              setShowfile(false);
            }}
          >
            <CloseIcon />{" "}
          </Button> */}
      </>
      {/* ) : (
        <Button>
          {" "}
          <AttachFileIcon
            className="attach-file"
            onClick={() => {
              setShowfile(true);
            }}
          />{" "}
        </Button>
      )} */}

      <input
        className="new_message_input"
        value={message}
        onChange={handleTextChange}
        placeholder="Enter New Message ..."
        type="text"
        onKeyDown={handleKeyPress}
      />
      {/* <button 
        className="new_message_button"
        alt="New Message"
        onClick={sendMessage}
        >Submit</button> */}
      <SendRoundedIcon className="new_message_button" onClick={sendMessage} />
    </div>
  );
}
