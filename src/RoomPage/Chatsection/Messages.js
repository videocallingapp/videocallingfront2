import React, { useRef, useEffect } from "react";
import { connect } from "react-redux";
import DescriptionIcon from "@material-ui/icons/Description";

const Message = ({
  author,
  content,
  sameAuthor,
  messageCreatedByMe,
  time,
  type,
  name,
}) => {
  const alignClass = messageCreatedByMe
    ? "message_align_right"
    : "message_align_left";

  const authorText = messageCreatedByMe ? "You" : author;

  const contentAdditionalStyles = messageCreatedByMe
    ? "message_right_styles"
    : "message_left_styles";

  return (
    <div className={`message_container ${alignClass}`}>
      {!sameAuthor && <p className="message_title">{authorText} </p>}
      <div className={`message_content ${contentAdditionalStyles}`}>
        {type === "file" ? (
          <a target="_blank" href={content} rel="noreferrer">
            <DescriptionIcon />
            <div> {name} </div>
          </a>
        ) : (
          <div>{content}</div>
        )}
        <div className="message-time-display">{time}</div>
      </div>
    </div>
  );
};

const Messages = ({ messages, socketId }) => {
  const scrollRef = useRef();

  useEffect(() => {
    scrollRef.current.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  console.log(messages);
  return (
    <div className="messages_container">
      {messages.map((message, index) => {
        const sameAuthor =
          index > 0 && message.socketId === messages[index - 1].socketId;
        const messageCreatedByMe = socketId === message.socketId;
        return (
          <Message
            key={`${index}`}
            author={message.identity}
            content={message.content}
            sameAuthor={sameAuthor}
            messageCreatedByMe={messageCreatedByMe}
            time={message.time}
            type={message.type}
            name={message.name}
          />
        );
      })}
      {/* <div>Hi</div> */}
      <div ref={scrollRef}></div>
    </div>
  );
};

const mapStoreStateToProps = (state) => {
  return {
    ...state,
  };
};

export default connect(mapStoreStateToProps)(Messages);
