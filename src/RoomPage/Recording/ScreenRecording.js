import React, { useState } from "react";
import RecordRTC from "recordrtc";
import { saveAs } from "file-saver";
import "./recording.css";
import VideocamIcon from "@mui/icons-material/Videocam";
import Tooltip from "@mui/material/Tooltip";

let media_recorder;
let localstream, stream;
let blobs_recorded = [];
const video = document.querySelector("video");
const canvas = (window.canvas = document.querySelector("canvas"));
export default function ScreenRecording() {
  const [show, setShow] = useState(false);
  const [showblink, setShowblink] = useState(false);
  const [message, setMessage] = useState("start recording");
  async function localrecording() {
    console.log("starting");
    localstream = await navigator.mediaDevices.getDisplayMedia({
      video: {
        // cursor: 'never',
        // displaySurface: 'monitor',
        aspectRatio: 1.7777777777777777,
        frameRate: 90,
        height: 1080,
        // logicalSurface: true,
        // resizeMode: "crop-and-scale",
        width: 1920,
        facingMode: "user",
      },
      audio: {
        echoCancellation: true,
        sampleRate: 100,
        volume: 1.0,
        restrictOwnAudio: true,
        noiseSuppression: true,
        autoGainControl: true,
      },
    });
    media_recorder = new MediaRecorder(localstream, { mimeType: "video/webm" });
    media_recorder.addEventListener("dataavailable", function (e) {
      blobs_recorded.push(e.data);
    });

    // event : recording stopped & all blobs sent
    media_recorder.addEventListener("stop", function () {
      // create local object URL from the recorded video blobs
      let video_local = URL.createObjectURL(
        new Blob(blobs_recorded, { type: "video/mp4" })
      );
      console.log(video_local);
      setMessage("start recording");
      saveAs(video_local, "recording.mp4");
    });

    setShow(true);
    setMessage("stop recording");
    add();
  
    localstream.getVideoTracks()[0].onended = function () {
      setMessage("start recording");
      localStoping();
      setShow(false);
      setShowblink(false);
    };
    // start recording with each recorded blob having 1 second video
    media_recorder.start(100);
  }

  // async function startRecording() {

  //   await navigator.mediaDevices.getDisplayMedia({
  //     audio: true,
  // video: { width: 1920, height: 1080 }
  //   }).then(async function (stream) {
  //     // {'echoCancellation': true}
  //     navigator.mediaDevices.getUserMedia({ 'audio': true, }).then((audio) => {
  //       localstream=stream;
  //       localaudio=audio;
  //       screenRecording(stream, null);
  //       add();
  //     })
  //       .catch((err) => {
  //         console.log('error in accessing microphone: ' + err);
  //         screenRecording(stream);
  //       })
  //   })
  //     .catch((err) => {
  //       console.log('====================================');
  //       console.log('error', err);
  //       console.log('====================================');
  //     })

  // }

  // function screenRecording(stream, audio = null) {
  //   if (audio) {
  //     recorder = RecordRTC([stream, audio], {
  //       type: 'video/mp4',
  //       bitsPerSecond: 128000,
  //       timeSlice: 1000,
  //       disableLogs: true,
  //       frameInterval: 90,
  //       video: { width: 1920, height: 1080 },
  //       sampleRate: 96000,
  //       // used by StereoAudioRecorder
  //       // the range 22050 to 96000.
  //       // let us force 16khz recording:
  //       desiredSampRate: 16000,

  //       // used by StereoAudioRecorder
  //       // Legal values are (256, 512, 1024, 2048, 4096, 8192, 16384).
  //       bufferSize: 16384,

  //       // used by StereoAudioRecorder
  //       // 1 or 2
  //       numberOfAudioChannels: 2,

  //       // used by WebAssemblyRecorder
  //       frameRate: 60,

  //       // used by WebAssemblyRecorder
  //       bitrate: 128000,

  //       // used by MultiStreamRecorder - to access HTMLCanvasElement
  //       // elementClass: 'multi-streams-mixer'
  //     });

  //   }

  //   else {
  //     recorder = RecordRTC([stream], {
  //       type: 'video',
  //       bitsPerSecond: 128000,
  //       frameInterval: 60,
  //       video: { width: 1920, height: 1080 },
  //       sampleRate: 96000,
  //       // used by StereoAudioRecorder
  //       // the range 22050 to 96000.
  //       // let us force 16khz recording:
  //       desiredSampRate: 16000,
  //       // used by StereoAudioRecorder
  //       // Legal values are (256, 512, 1024, 2048, 4096, 8192, 16384).
  //       bufferSize: 16384,

  //       // used by StereoAudioRecorder
  //       // 1 or 2
  //       numberOfAudioChannels: 2,

  //       // used by WebAssemblyRecorder
  //       frameRate: 60,
  //       // used by WebAssemblyRecorder
  //       bitrate: 128000,
  //       videoBitsPerSecond: 128000,

  //       // used by MultiStreamRecorder - to access HTMLCanvasElement
  //       // elementClass: 'multi-streams-mixer'
  //     });

  //   }

  //   recorder.startRecording();
  //   setShow(true);
  //   setMessage('stop recording');
  //   stream.getVideoTracks()[0].onended = function () {
  //     stopRecording();
  //     setShow(false);
  //     setShowblink(false)
  //   };

  //   // stream.getTracks().forEach((track) => {
  //   //   track.addEventListener('ended', () => {
  //   //     stopRecording();
  //   //   }, false);
  //   //   track.addEventListener('inactive', () => {
  //   //    console.log('inactive');
  //   //    stopRecording();
  //   //   }, false);
  //   // });

  // }
  function add() {
    setShowblink(!showblink);
  }

  // function stopRecording() {
  //   media_recorder.stop();
  //   setMessage('start recording');
  //   add();
  //     setShow(false);
  // }
  function localStoping() {
    console.log("====================================");
    console.log("stopping recording");
    console.log("====================================");
    localstream.getTracks().forEach((t) => t.stop());
    // localaudio.getTracks().forEach((t) => t.stop());
    add();
    setShow(false);
    media_recorder.stop();
  }

  // function stopRecording() {
  //   localstream.getTracks().forEach((t) => t.stop());
  //   // localaudio.getTracks().forEach((t) => t.stop());
  //   setMessage('start recording');

  //   const getFileName = (fileExtension) => {
  //     var d = new Date();
  //     var year = d.getFullYear();
  //     var month = d.getMonth();
  //     var date = d.getDate();
  //     return 'ScreenRecord-' + year + month + date + ' .' + fileExtension;
  //   }
  //   recorder.stopRecording(function () {
  //     let recorderBlob = recorder.getBlob();
  //     if (recorderBlob) {
  //       var blob = recorderBlob;
  //       saveAs(blob, getFileName('mp4'));
  //     }
  //   });
  //   add();
  //   setShow(false);

  // }

  function handleClickScreenRecording() {
    if (!showblink) {
      // startRecording();
      localrecording();
    } else {
      // stopRecording();
      localStoping();
    }
  }

  return (
    <div className="recording-icon" onClick={handleClickScreenRecording}>
      <div className={` ${showblink ? "blink-red-circle" : ""}`}></div>
      <Tooltip title={message} placement="top">
        <VideocamIcon className="video-record" style={{ color: "white" }} />
      </Tooltip>
    </div>
  );
}
