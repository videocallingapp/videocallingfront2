import React from "react";
import store from "../../store/store";
import { setActiveConversation } from "../../store/actions";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import moment from "moment";

import * as wss from "../../utils/wss";

function SingleParticipant(props) {
  const {
    lastItem,
    participant,
    identity,
    setActiveConversationAction,
    socketId,
  } = props;
  const handleOpenActiveChatBox = () => {
    if (participant.socketId !== socketId) {
      try {
        store.dispatch(setActiveConversation(participant));
      } catch (e) {
        console.log(e);
      }
    }
  };
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  console.log(store.getState().isRoomHost);

  return (
    <>
      <div className="participants_container2">
        <p className="participants_paragraph" onClick={handleOpenActiveChatBox}>
          {identity}
        </p>
        {store.getState().isRoomHost &&
          participant.socketId != store.getState().socketId && (
            <div className="hostControl">
              <Button
                id="basic-button"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
              >
                <MoreVertIcon />
              </Button>

              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem
                  onClick={() => {
                    wss.kickParticipant({
                      recieverSocketId: participant.socketId,
                      identity: identity,
                      messageContent: "Kick Participant",
                      time: moment().format("LT"),
                    });
                    handleClose();
                  }}
                >
                  Kick
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    wss.stopVideo({
                      recieverSocketId: participant.socketId,
                      identity: identity,
                      messageContent: "stop Video",
                      time: moment().format("LT"),
                    });
                    handleClose();
                  }}
                >
                  Stop Video
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    wss.muteAudio({
                      recieverSocketId: participant.socketId,
                      identity: identity,
                      messageContent: "mute user",
                      time: moment().format("LT"),
                    });
                    handleClose();
                  }}
                >
                  Mute Audio
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    wss.stopScreenShare({
                      recieverSocketId: participant.socketId,
                      identity: identity,
                      messageContent: "stop screen share",
                      time: moment().format("LT"),
                    });
                    handleClose();
                  }}
                >
                  Stop Screen Share
                </MenuItem>
              </Menu>
            </div>
          )}
      </div>
      {!lastItem && <span className="participants_seperator_line"></span>}
    </>
  );
}
export default SingleParticipant;
