import React, { useState } from "react";
import SendIcon from "@mui/icons-material/Send";
import * as wss from "../../../utils/wss";
import moment from "moment";
import { Button } from "@material-ui/core";
import Emoji from "../../../Emoji/Emoji";
import CloseIcon from "@material-ui/icons/Close";
import EmojiEmotionsOutlinedIcon from "@material-ui/icons/EmojiEmotionsOutlined";

export default function NewMessage({ activeConversation, identity }) {
  const [message, setMessage] = useState("");
  const [emojiTemplate, setEmojiTemplate] = useState(false);
  function handleKeyPress(e) {
    if (e.key === "Enter" && message) {
      e.preventDefault();
      sendMessage();
    }
  }

  function setEmoji(Emoji) {
    setMessage(message + Emoji);
  }
  function sendMessage() {
    if (message) {
      wss.sendDirectMessage({
        recieverSocketId: activeConversation.socketId,
        identity: identity,
        messageContent: message,
        time: moment().format("LT"),
      });
      setMessage("");
    }
  }

  return (
    <div className="new_message_container new_message_direct_border">
      {emojiTemplate ? (
        <>
          <Emoji setEmoji={setEmoji} />
          <Button
            onClick={() => {
              setEmojiTemplate(false);
            }}
          >
            <CloseIcon />
          </Button>
        </>
      ) : (
        <Button
          onClick={() => {
            setEmojiTemplate(true);
          }}
        >
          <EmojiEmotionsOutlinedIcon />
        </Button>
      )}

      <input
        className="new_message_input"
        value={message}
        onChange={(e) => {
          setMessage(e.target.value);
        }}
        type="text"
        placeholder="Enter a new message"
        onKeyDown={handleKeyPress}
      />
      <SendIcon className="new_message_button " onClick={sendMessage} />
    </div>
  );
}
