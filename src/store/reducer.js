import Actions from "./actions";
const initState = {
  identity: " ",
  isRoomHost: false,
  connectOnlyWithAudio: true,
  roomId: null,
  showOverlay: true,
  participants: [],
  messages: [],
  connectOnlyWithVideo: true,
  activeConversation: null,
  directChatHistory: [],
  socketId: null,
  streams: [],
  isUserJoined: false,
  waitInLobby: true,
  allowUser: {
    value: false,
    name: "",
    socketId: "",
  },
  blockVideo: false,
  blockAudio: false,
  blockScreenShare: false,
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case Actions.SET_BLOCKVIDEO:
      return { ...state, blockVideo: action.blockVideo };
    case Actions.SET_BLOCKAUDIO:
      return { ...state, blockAudio: action.blockAudio };
    case Actions.SET_BLOCKSCREENSHARE:
      return { ...state, blockScreenShare: action.blockScreenShare };
    case Actions.SET_IS_ROOM_HOST:
      return {
        ...state,
        isRoomHost: action.isRoomHost,
      };
    case Actions.SET_CONNECT_ONLY_WITH_AUDIO:
      return {
        ...state,
        connectOnlyWithAudio: action.onlyWithAudio,
      };
    case Actions.SET_CONNECT_ONLY_WITH_VIDEO:
      return {
        ...state,
        connectOnlyWithVideo: action.onlyWithVideo,
      };
    case Actions.SET_ROOM_ID:
      return {
        ...state,
        roomId: action.roomId,
      };
    case Actions.SET_IDENTITY:
      return {
        ...state,
        identity: action.identity,
      };
    case Actions.SET_SHOW_OVERLAY:
      return {
        ...state,
        showOverlay: action.showOverlay,
      };
    case Actions.SET_PARTICIPANTS:
      return {
        ...state,
        participants: action.participants,
      };
    case Actions.SET_STREAMS:
      return {
        ...state,
        streams: [...state.streams, action.streams],
      };
    case Actions.SET_MESSAGES:
      return {
        ...state,
        messages: action.messages,
      };
    case Actions.SET_ACTIVE_CONVERSATION:
      return {
        ...state,
        activeConversation: action.activeConversation,
      };
    case Actions.REMOVE_STREAMS:
      const newStream = state.streams.filter(
        (stream) => stream.socketId !== action.socketId
      );
      return {
        ...state,
        streams: newStream,
      };
    case Actions.SET_DIRECT_CHAT_HISTORY:
      return {
        ...state,
        directChatHistory: action.directChatHistory,
      };
    case Actions.SET_SOCKET_ID:
      return {
        ...state,
        socketId: action.socketId,
      };
    case Actions.SET_ISUSERJOINED:
      return {
        ...state,
        isUserJoined: action.data,
      };
    case "ALLOW_USER":
      return {
        ...state,
        allowUser: action.allowUser,
      };
    default:
      return state;
  }
};
export default reducer;
