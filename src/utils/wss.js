import io from "socket.io-client";
import store from "../store/store";
import * as WebRTCHandler from "./WebRTCHandler";
import {
  setRoomId,
  setParticipants,
  setSocketId,
  isUserJoined,
  setBlockVideo,
  setBlockAudio,
  setBlockScreenShare,
} from "../store/actions";
import { server } from "../baseurl";
import { useNavigate } from "react-router-dom";
import { appendNewMessage } from "./DirectMessages";
import url from "../baseurl";

const Server = server;
let socket = null;

export const connectWithSocketIOServer = () => {
  socket = io(Server);
  socket.on("connect", () => {
    console.log("successfully connected with socket io server", socket.id);
    store.dispatch(setSocketId(socket.id));
  });
  socket.on("room-id", (data) => {
    // console.log(data)
    const { roomId } = data;
    store.dispatch(setRoomId(roomId));
  });
  socket.on("room-update", (data) => {
    const { connectedUsers } = data;
    store.dispatch(setParticipants(connectedUsers));
  });
  socket.on("conn-prepare", (data) => {
    const { connUserSocketId } = data;
    WebRTCHandler.prepareNewPeerConnection(connUserSocketId, false);
    socket.emit("conn-init", { connUserSocketId: connUserSocketId });
  });
  socket.on("conn-signal", (data) => {
    WebRTCHandler.handleSignalingData(data);
  });
  socket.on("conn-init", (data) => {
    const { connUserSocketId } = data;
    WebRTCHandler.prepareNewPeerConnection(connUserSocketId, true);
  });
  socket.on("user-disconnected", (data) => {
    WebRTCHandler.removePeerConnection(data);
  });
  socket.on("new-message", (data) => {
    console.log("new message come ", data);
    WebRTCHandler.handleNewMessage(data);
  });
  socket.on("previous-chat", (data) => {
    WebRTCHandler.handlePreviousMessage(data);
  });
  socket.on("direct-message", (data) => {
    console.log("new direct message come ", data);
    appendNewMessage(data);
  });
  socket.on("mute-audio", (data) => {
    console.log("mute audio recieved");
    WebRTCHandler.muteAudio();
    store.dispatch(setBlockAudio(true));
  });
  socket.on("stop-video", (data) => {
    console.log("mute audio recieved");
    WebRTCHandler.stopVideo();
    store.dispatch(setBlockVideo(true));
  });
  socket.on("stop-screenShare", (data) => {
    console.log("stop screen share recieved");
    WebRTCHandler.stopScreenShare();
    store.dispatch(setBlockScreenShare(true));
    // const constraits = {
    //   audio: false,
    //   video: true,
    // };

    // navigator.mediaDevices
    //   .getDisplayMedia(constraits)
    //   .then((stream) => {
    //     WebRTCHandler.stopScreenShare();

    //     stream.getVideoTracks()[0].onended = function () {
    //       WebRTCHandler.stopScreenShare();

    //       console.log("screen recording stopped");
    //     };
    //     // setIsScreenSharingActive(!isScreenSharingActive);
    //   })
    //   .catch((error) => {
    //     console.log("====================================");
    //     console.log("error in screen sharing", error);
    //     console.log("====================================");
    //     return;
    //   });
    document.getElementById("screenShareButton").click();
  });
  socket.on("kick-participant", (data) => {
    console.log("mute audio recieved");
    window.location.href = url + "end";
  });
  socket.on("ask-host-to-join", (data) => {
    console.log("ask host to joined");
    console.log(store.getState().isRoomHost);
    store.dispatch({
      type: "ALLOW_USER",
      allowUser: { value: true, name: data.identity, socketId: data.socketId },
    });
  });
};
export const createNewRoom = (identity) => {
  const data = {
    identity: identity,
  };
  socket.emit("create-new-room", data);
};
export const joinRoom = (identity, roomId) => {
  const data = {
    identity: identity,
    roomId: roomId,
  };
  socket.emit("join-room", data);
};
export const signalPeerData = (data) => {
  socket.emit("conn-signal", data);
};
export const sendMessage = (message) => {
  socket.emit("send-message", message);
};
export const sendDirectMessage = (data) => {
  socket.emit("direct-message", data);
};

export const muteAudio = (data) => {
  socket.emit("mute-audio", data);
  console.log("emitting mute audio");
};

export const stopVideo = (data) => {
  socket.emit("stop-video", data);
  console.log("emitting stop video");
};

export const stopScreenShare = (data) => {
  socket.emit("stop-screenShare", data);
  console.log("emitting stop screen share");
};

export const kickParticipant = (data) => {
  socket.emit("kick-participant", data);
  console.log("emitting kick participant");
};

export const askHostToJoin = (data) => {
  socket.emit("ask-host-to-join", data);
  console.log("ask-host-to-join");
};
//hi
